import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstArenaFighter = {
      fighter: firstFighter,
      health: firstFighter.health,
      blockCriticalHit: false,
      indicator: document.getElementById('left-fighter-indicator')
    };
    const secondArenaFighter = {
      fighter: secondFighter,
      health: secondFighter.health,
      blockCriticalHit: false,
      indicator: document.getElementById('right-fighter-indicator')
    };
    const pressedKeys = new Set();

    document.addEventListener('keydown', (event) => {
      pressedKeys.add(event.code);

      switch (event.code) {
        case controls.PlayerOneAttack:
          if (!pressedKeys.has(controls.PlayerOneBlock) && !pressedKeys.has(controls.PlayerTwoBlock)) {
            attackAction(firstArenaFighter, secondArenaFighter);
          }
          break;
        case controls.PlayerTwoAttack:
          if (!pressedKeys.has(controls.PlayerTwoBlock) && !pressedKeys.has(controls.PlayerOneBlock)) {
            attackAction(secondArenaFighter, firstArenaFighter);
          }
          break;
      }

      if (!firstArenaFighter.blockCriticalHit &&
        controls.PlayerOneCriticalHitCombination.includes(event.code) &&
        checkKeysCriticalHit(controls.PlayerOneCriticalHitCombination, pressedKeys)) {
        attackAction(firstArenaFighter, secondArenaFighter, true);
      }
      if (!secondArenaFighter.blockCriticalHit &&
        controls.PlayerTwoCriticalHitCombination.includes(event.code) &&
        checkKeysCriticalHit(controls.PlayerTwoCriticalHitCombination, pressedKeys)) {
        attackAction(secondArenaFighter, firstArenaFighter, true);
      }
    });

    document.addEventListener('keyup', (event) => {
      pressedKeys.delete(event.code);
    });

    function attackAction(attacker, defender, isCritical = false) {
      let damage;
      if (isCritical) {
        damage = getCriticalDamage(attacker.fighter);
        attacker.blockCriticalHit = true;
        setTimeout(() => {
          attacker.blockCriticalHit = false;
        }, 10000);
      } else {
        damage = getDamage(attacker.fighter, defender.fighter);
      }
      defender.health -= damage;
      setFighterHealthBar(defender);
      if (defender.health <= 0) resolve(attacker.fighter);
    }
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

export function getCriticalDamage(fighter) {
  return fighter.attack * 2;
}

function checkKeysCriticalHit(keys, pressed) {
  for (let key of keys) {
    if (!pressed.has(key)) {
      return false;
    }
  }
  return true;
}

function setFighterHealthBar(player) {
  let percent = player.health * 100 / player.fighter.health;
  if (percent < 0) percent = 0;
  player.indicator.style.width = `${percent}%`;
}
